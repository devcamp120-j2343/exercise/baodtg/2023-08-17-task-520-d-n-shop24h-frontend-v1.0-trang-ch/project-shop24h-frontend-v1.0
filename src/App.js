import './App.css';
import { Footer } from './components/Footer.component';
import Header from './components/Header.component';
import { HomePage } from './components/HomePage.component';

function App() {
  return (
    < >
      <Header />
      <HomePage />
      <Footer />
    </>
  );
}

export default App;
