import { ChildHomePage } from "./components/childHomepage/ChildHomePage.components";
import { LastestProducts } from "./components/childHomepage/LastestProducts.component";


const routes = [
    {
        path: "/",
        element: <ChildHomePage />
    },
    {
        path: "/product",
        element: <LastestProducts />
    }


];

export default routes;