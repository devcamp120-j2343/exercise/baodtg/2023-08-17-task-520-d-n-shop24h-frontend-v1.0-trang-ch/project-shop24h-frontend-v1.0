import { Box, Button, Card, CardActions, CardContent, Container, Grid, Link, Typography } from "@mui/material"
import macbook from '../../assets/images/home-page-images/csm_Apple_WWDC23_MacBook_Air_15_in_hero_230605_8979d76b86.jpg'
import iphone14 from '../../assets/images/home-page-images/50230-98752-iPhone-1-xl.jpg'
import appleEvent from '../../assets/images/home-page-images/Apple-Event-2023-Template.jpg'
import iphone14Promax from '../../assets/images/home-page-images/Apple-iPhone-14-Pro-iPhone-14-Pro-Max-hero-220907_Full-Bleed-Image.jpg.large.jpg'
import appleWatch from '../../assets/images/home-page-images/gps-lte__gi7uzrvkt5e2_og.png'
import IpadPro from '../../assets/images/home-page-images/ipad-pro_overview__glcw458o4byq_og.png'
import iPhone from '../../assets/images/home-page-images/1687750528205-iphone-15-pro.webp'
import mac from '../../assets/images/home-page-images/macbook-air-m1-2020-gray-600x600.jpg'
import ipad from '../../assets/images/home-page-images/ipad small image.jpeg'
import watch from '../../assets/images/home-page-images/Apple-Watch-Ultra small image.jpg'
import airpods from '../../assets/images/home-page-images/airpods small image.webp'
import sac from '../../assets/images/home-page-images/-Op6Ug8.webp'
import { ViewAll } from "./ViewAll.component"


export const ChildHomePage = () => {
    const products = [
        {
            images: iPhone,
            name: 'iPhone',

        },
        {
            images: mac,
            name: 'Mac',

        },
        {
            images: ipad,
            name: 'iPad',

        },
        {
            images: watch,
            name: 'Watch',

        },
        {
            images: airpods,
            name: 'Airpods',

        },
        {
            images: sac,
            name: 'accessory',

        },
    ];
    return (
        <Container maxWidth>
            <Grid container>
                <Grid container item lg={12} sm={12}
                    sx={{
                        width: "100%",
                        height: "100vh",
                        backgroundImage: `url(${macbook})`,
                        backgroundPosition: "center",
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        mt: 3
                    }}
                >
                    <Grid item lg={6} sm={6} ml={'50%'}>
                        <Box textAlign={'center'} pt={'50%'}>
                            <Typography variant="h2" sx={{ fontWeight: 700 }}>
                                Macbook Air 15"

                            </Typography>
                            <Typography variant="h4" sx={{ fontWeight: 500 }}>
                                Impressively big. Impossibly thin.

                            </Typography>
                            <Box width={'25%'} mx={'auto'} display={'flex'} justifyContent={'space-evenly'} >
                                <Link underline="hover" variant="h6" >Learn more ></Link>
                                <Link underline="hover" variant="h6">Buy ></Link>
                            </Box>


                        </Box>
                    </Grid>
                </Grid>
                <Grid container item lg={12} sm={12}
                    sx={{ py: 5, mt: 3, background: '#FAFAFA' }}
                >
                    <Grid item lg={12} sm={6} mx={'auto'}>
                        <Box textAlign={'center'}>
                            <Typography variant="h2" sx={{ fontWeight: 700 }}>
                                iPhone 14
                            </Typography>
                            <Typography variant="h4" sx={{ fontWeight: 500 }}>
                                Wonderfull.

                            </Typography>
                            <Box width={'25%'} mx={'auto'} display={'flex'} justifyContent={'space-evenly'} >
                                <Link underline="hover" variant="h6" >Learn more ></Link>
                                <Link underline="hover" variant="h6">Buy ></Link>
                            </Box>


                        </Box>
                    </Grid>
                    <Grid item lg={12} sm={12} mx={'auto'} sx={{ textAlign: 'center' }}>
                        <img src={iphone14}></img>
                    </Grid>
                </Grid>

                <Grid container item lg={12} sm={12} spacing={2}

                >
                    <Grid item lg={6} sm={6}>
                        <img src={appleEvent} width={'100%'}></img>
                    </Grid>
                    <Grid item lg={6} sm={6}>
                        <img src={iphone14Promax} width={'100%'}></img>
                    </Grid>
                    <Grid item lg={6} sm={6}>
                        <img src={IpadPro} width={'100%'}></img>
                    </Grid>
                    <Grid item lg={6} sm={6}>
                        <img src={appleWatch} width={'100%'}></img>
                    </Grid>

                </Grid>
                <Grid container mt={3}>
                    <Grid item lg={12} sm={12} sx={{ background: '#f5f5f7' }}>
                        <Box sx={{ width: '90%', mt: 4, mx: 'auto', display: 'flex', flexWrap: 'wrap', }}>
                            {products.map((product, i) =>
                                <Card sx={{ width: '15rem', borderRadius: '10px', mx: 2, my: 2, boxShadow: '10px 5px 5px grey' }}>
                                    <CardContent sx={{ height: '12rem' }} >
                                        <img src={`${product.images}`} alt={product.name} width={'100%'}></img>
                                    </CardContent>
                                    <CardActions >
                                        <Button sx={{ mx: 'auto', fontWeight: 700, color: 'black' }} size="small">{product.name}</Button>
                                    </CardActions>
                                    <CardContent sx={{ textAlign: "center", py: 0 }}>
                                        <Typography sx={{ color: 'blue', fontWeight: 700 }} >
                                            {product.price}
                                        </Typography>
                                    </CardContent>

                                </Card>
                            )}
                        </Box>
                    </Grid>

                </Grid>
                <ViewAll />
            </Grid>

        </Container >
    )

}