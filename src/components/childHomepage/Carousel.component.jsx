import Carousel from "react-material-ui-carousel"
import banner1 from '../../assets/images/banner/banner iphone 14 Pro Max T9_Banner PC.jpg'
import banner2 from '../../assets/images/banner/banner ipad gen9 T9_Banner PC.jpg'
import banner3 from '../../assets/images/banner/banner watch T9_Banner PC.jpg'
import banner4 from '../../assets/images/banner/banner macbook air t9_Banner PC.jpg'
import banner5 from '../../assets/images/banner/banner apple pay_Banner PC.jpg'

export const SlideBanner = () => {
    var items =
        [banner1, banner2, banner3, banner4, banner5]

    console.log(items)
    return (
            <Carousel sx={{ mt: 2 }} >
                {
                    items.map((item, i) =>
                        <img
                            key={i}
                            src={`${item} ` }
                            width={'100%'}
                            alt="banner"
                        />
                    )

                }
            </Carousel>

    )
}

