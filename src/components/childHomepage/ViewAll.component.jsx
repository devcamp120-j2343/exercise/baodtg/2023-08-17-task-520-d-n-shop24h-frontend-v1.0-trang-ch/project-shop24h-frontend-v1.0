import { Button, Container, Grid } from "@mui/material"

export const ViewAll = () => {
    return (
        <Container >
            <Grid container mt={5} mb={5} justifyContent={'center'}>
                <Grid item>
                    <Button href="/product" variant="contained" sx={{ background: '#EDF0F5', color: 'black', fontWeight: 700 }} >View All</Button>

                </Grid>
            </Grid>
        </Container>
    )
}