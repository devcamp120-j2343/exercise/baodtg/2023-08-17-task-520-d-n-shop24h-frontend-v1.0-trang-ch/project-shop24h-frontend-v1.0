import { Box, Button, Card, CardActions, CardContent, Container, Grid, Typography } from "@mui/material"
import product1 from '../../assets/images/products/0007808_iphone-14-pro-max-128gb_240.png'
import ipad1 from '../../assets/images/products/0010883_ipad-pro-m1-129-inch-wifi-cellular-512gb_240.webp'
import watch1 from '../../assets/images/products/0008913_apple-watch-ultra-lte-49mm-alpine-loop-size-s_240.png'
import airpods1 from '../../assets/images/products/0012005_airpods-max_240.webp'
export const LastestProducts = () => {
    const products = [
        {
            images: product1,
            name: 'Iphone 14 Promax 128gb',
            price: '26.150.000'
        },
        {
            images: product1,
            name: 'Iphone 15 Promax 128gb',
            price: '26.150.000'
        },
        {
            images: product1,
            name: 'Iphone 15 Promax 128gb',
            price: '26.150.000'
        },
        {
            images: product1,
            name: 'Iphone 15 Promax 128gb',
            price: '26.150.000'
        }

    ];

    const ipads = [
        {
            images: ipad1,
            name: 'iPad Pro M1 12.9 inch WiFi Cellular 512GB',
            price: '35.450.000'
        },
        {
            images: ipad1,
            name: 'iPad Pro M1 12.9 inch WiFi Cellular 512GB',
            price: '35.450.000'
        },
        {
            images: ipad1,
            name: 'iPad Pro M1 12.9 inch WiFi Cellular 512GB',
            price: '35.450.000'
        },
        {
            images: ipad1,
            name: 'iPad Pro M1 12.9 inch WiFi Cellular 512GB',
            price: '35.450.000'
        }

    ];

    const watches = [
        {
            images: watch1,
            name: 'iPad Pro M1 12.9 inch WiFi Cellular 512GB',
            price: '35.450.000'
        },
        {
            images: watch1,
            name: 'iPad Pro M1 12.9 inch WiFi Cellular 512GB',
            price: '35.450.000'
        },
        {
            images: watch1,
            name: 'iPad Pro M1 12.9 inch WiFi Cellular 512GB',
            price: '35.450.000'
        },
        {
            images: watch1,
            name: 'iPad Pro M1 12.9 inch WiFi Cellular 512GB',
            price: '35.450.000'
        }

    ];

    const airpods = [
        {
            images: airpods1,
            name: 'iPad Pro M1 12.9 inch WiFi Cellular 512GB',
            price: '35.450.000'
        },
        {
            images: airpods1,
            name: 'iPad Pro M1 12.9 inch WiFi Cellular 512GB',
            price: '35.450.000'
        },
        {
            images: airpods1,
            name: 'iPad Pro M1 12.9 inch WiFi Cellular 512GB',
            price: '35.450.000'
        },
        {
            images: airpods1,
            name: 'iPad Pro M1 12.9 inch WiFi Cellular 512GB',
            price: '35.450.000'
        }

    ]
    return (
        <Container maxWidth >
            <Grid container justifyContent='center' mt={3}>
                <Grid item >
                    <Typography variant="h4" sx={{ fontWeight: 900 }} >
                        iPhone
                    </Typography>
                </Grid>
            </Grid>
            <Box sx={{ width: '80%', mt: 4, mx: 'auto', display: 'flex', flexWrap: 'wrap', }}>
                {products.map((product, i) =>
                    <Card sx={{ width: '18rem', borderRadius: '10px', mx: 5, my: 2 }}>
                        <CardContent >
                            <img src={`${product.images}`} alt={product.name}></img>
                        </CardContent>
                        <CardActions >
                            <Button sx={{ mx: 'auto', fontWeight: 700, color: 'black' }} size="small">{product.name}</Button>
                        </CardActions>
                        <CardContent sx={{ textAlign: "center", py: 0 }}>
                            <Typography sx={{ color: 'blue', fontWeight: 700 }} >
                                {product.price}
                            </Typography>
                        </CardContent>

                    </Card>
                )}
            </Box>
            <Grid container justifyContent='center' mt={3}>
                <Grid item >
                    <Typography variant="h4" sx={{ fontWeight: 900 }} >
                        iPad
                    </Typography>
                </Grid>
            </Grid>
            <Box sx={{ width: '80%', mt: 4, mx: 'auto', display: 'flex', flexWrap: 'wrap', }}>
                {ipads.map((product, i) =>
                    <Card sx={{ width: '18rem', borderRadius: '10px', mx: 5, my: 2 }}>
                        <CardContent >
                            <img src={`${product.images}`} alt={product.name}></img>
                        </CardContent>
                        <CardActions >
                            <Button sx={{ mx: 'auto', fontWeight: 700, color: 'black' }} size="small">{product.name}</Button>
                        </CardActions>
                        <CardContent sx={{ textAlign: "center", py: 0 }}>
                            <Typography sx={{ color: 'blue', fontWeight: 700 }} >
                                {product.price}
                            </Typography>
                        </CardContent>

                    </Card>
                )}
            </Box>

            <Grid container justifyContent='center' mt={3}>
                <Grid item >
                    <Typography variant="h4" sx={{ fontWeight: 900 }} >
                        Apple Watch
                    </Typography>
                </Grid>
            </Grid>
            <Box sx={{ width: '80%', mt: 4, mx: 'auto', display: 'flex', flexWrap: 'wrap', }}>
                {watches.map((product, i) =>
                    <Card sx={{ width: '18rem', borderRadius: '10px', mx: 5, my: 2 }}>
                        <CardContent >
                            <img src={`${product.images}`} alt={product.name}></img>
                        </CardContent>
                        <CardActions >
                            <Button sx={{ mx: 'auto', fontWeight: 700, color: 'black' }} size="small">{product.name}</Button>
                        </CardActions>
                        <CardContent sx={{ textAlign: "center", py: 0 }}>
                            <Typography sx={{ color: 'blue', fontWeight: 700 }} >
                                {product.price}
                            </Typography>
                        </CardContent>

                    </Card>
                )}
            </Box>
            <Grid container justifyContent='center' mt={3}>
                <Grid item >
                    <Typography variant="h4" sx={{ fontWeight: 900 }} >
                        Tai nghe
                    </Typography>
                </Grid>
            </Grid>
            <Box sx={{ width: '80%', mt: 4, mx: 'auto', display: 'flex', flexWrap: 'wrap', }}>
                {airpods.map((product, i) =>
                    <Card sx={{ width: '18rem', borderRadius: '10px', mx: 5, my: 2 }}>
                        <CardContent >
                            <img src={`${product.images}`} alt={product.name}></img>
                        </CardContent>
                        <CardActions >
                            <Button sx={{ mx: 'auto', fontWeight: 700, color: 'black' }} size="small">{product.name}</Button>
                        </CardActions>
                        <CardContent sx={{ textAlign: "center", py: 0 }}>
                            <Typography sx={{ color: 'blue', fontWeight: 700 }} >
                                {product.price}
                            </Typography>
                        </CardContent>

                    </Card>
                )}
            </Box>
            <Grid container item mt={5} mb={5} justifyContent={'center'}>
                <Grid item>
                    <Button href="/" variant="contained" sx={{ background: '#EDF0F5', color: 'black', fontWeight: 700 }} >Back</Button>

                </Grid>
            </Grid>
        </Container>
    )
}