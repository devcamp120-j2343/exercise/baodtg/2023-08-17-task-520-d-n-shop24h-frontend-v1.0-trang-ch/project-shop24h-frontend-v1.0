import { Box, Button, Container, Grid, IconButton, Link, List, ListItem, TextField, Typography } from "@mui/material"


export const Footer = () => {

    const shop = ['Store', 'Mac', 'IPad', 'IPhone', 'Watch', 'Vision', 'AirPods', 'TV & Home', 'Entertaiment', 'Support'];
    const wallet = ['Wallet', 'Apple Card', 'Apple Pay', 'Apple Cash']
    const entertaiment = ['Apple One', 'Apple TV+', 'Apple Music', 'Apple Arcard', 'Apple Fitness+', 'Apple New+', 'Apple Podcasts', 'Apple Books', 'Apple Store',]
    const appleStore = ['Find a Store', 'Genius Bar', 'Today at Apple', 'Apple Camp', 'Apple Store App', 'Certified Refurbished', 'Apple Trade In', 'Order Status', 'Shopping Help']
    return (
        <Container>
            <hr />
            <Grid container mt={3} >
                <Grid item sm={6} lg={2}>
                    <Box >
                        <Typography variant="h6" sx={{ fontWeight: 700 }}> Shop and Learn
                        </Typography>
                        <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                            {shop.map((value) => (
                                <ListItem
                                    key={value}
                                    disableGutters
                                >
                                    <Link href="#" underline="hover" sx={{ color: 'black' }}>{value}</Link>
                                </ListItem>
                            ))}
                        </List>
                    </Box>
                </Grid>
                <Grid item sm={6} lg={2}>
                    <Box >
                        <Typography variant="h6" sx={{ fontWeight: 700 }}>Apple Wallet</Typography>
                        <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                            {wallet.map((value) => (
                                <ListItem
                                    key={value}
                                    disableGutters
                                    secondaryAction={
                                        <IconButton aria-label="comment">
                                        </IconButton>
                                    }
                                >
                                    <Link href="#" underline="hover" sx={{ color: 'black' }}>{value}</Link>
                                </ListItem>
                            ))}
                        </List>
                    </Box>
                </Grid>
                <Grid item sm={6} lg={2}>
                    <Box >
                        <Typography variant="h6" sx={{ fontWeight: 700 }}>Entertaiment</Typography>
                        <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                            {entertaiment.map((value) => (
                                <ListItem
                                    key={value}
                                    disableGutters
                                    secondaryAction={
                                        <IconButton aria-label="comment">
                                        </IconButton>
                                    }
                                >
                                    <Link href="#" underline="hover" sx={{ color: 'black' }}>{value}</Link>
                                </ListItem>
                            ))}
                        </List>
                    </Box>
                </Grid>
                <Grid item sm={6} lg={2}>
                    <Box >
                        <Typography variant="h6" sx={{ fontWeight: 700 }}>Entertaiment</Typography>
                        <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper', }}>
                            {appleStore.map((value) => (
                                <ListItem
                                    key={value}
                                    disableGutters
                                    secondaryAction={
                                        <IconButton aria-label="comment">
                                        </IconButton>
                                    }
                                >
                                    <Link href="#" underline="hover" sx={{ color: 'black' }}>{value}</Link>
                                </ListItem>
                            ))}
                        </List>
                    </Box>
                </Grid>
                <Grid container item sm={6} lg={4}>
                    <Grid item sm={6} lg={12}>
                        <Box >
                            <Typography variant="h6" sx={{ fontWeight: 700 }}>NewsLetter</Typography>
                            <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper', }}>
                                <ListItem
                                    disableGutters
                                    secondaryAction={
                                        <IconButton aria-label="comment">
                                        </IconButton>
                                    }
                                >
                                    Heaven fruitful doesn't over lesser in days. Appear creeping
                                </ListItem>

                            </List>
                        </Box>
                    </Grid>
                    <Grid item sm={6} lg={12} sx={{ display: 'flex', alignItems: 'center' }}>
                        <TextField id="outlined-basic" label="Email Address"
                            variant="outlined" />
                        <Button sx={{ ml: 1, background: '#EDF0F5', color: 'black', height: '27%' }}

                            variant="contained" >subscribe</Button>
                    </Grid>
                </Grid>

                <Grid item sm={12} lg={12} mt={3}>
                    <Typography variant="p">More ways to shop: <Link href="#" underline="hover">Find an Apple Store</Link> or <Link href="#" underline="hover">other retailer</Link> near you. Or call 1‑800‑MY‑APPLE.</Typography>
                    <hr />
                </Grid>
                <Grid item sm={12} lg={12}>
                    <Typography variant="p">Copyright © 2023 Apple Inc. All rights reserved.</Typography>
                </Grid>
            </Grid>

        </Container >
    )
}