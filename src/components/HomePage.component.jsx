import { SlideBanner } from "./childHomepage/Carousel.component"
import { ChildHomePage } from "./childHomepage/ChildHomePage.components"
import { LastestProducts } from "./childHomepage/LastestProducts.component"
import { ViewAll } from "./childHomepage/ViewAll.component"
import { Routes, Route } from "react-router-dom";
import routes from "../routes";


export const HomePage = () => {

    return (
        <>
            <SlideBanner />
            <Routes>
                {
                    routes.map((route, index) => {
                        return <Route path={route.path} element={route.element} />
                    })
                }
            </Routes>
        </>
    )
}